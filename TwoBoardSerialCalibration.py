import serial
import time

portTagName = '/dev/tty.wchusbserial14340'
portAnchorName = '/dev/tty.wchusbserial14340'

serialTagPort = serial.Serial(port=portTagName, baudrate=115200,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)

serialAnchorPort = serial.Serial(port=portAnchorName, baudrate=115200,
                           bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE)


def convertToInt(mantissa_str):
    power_count = -1
    mantissa_int = 0
    for i in mantissa_str:
        mantissa_int += (int(i) * pow(2, power_count))
        power_count -= 1
    return (mantissa_int + 1)


def bin_to_float(input):
    result = int(input[1:9], 2)
    exponent_unbias = result - 127
    sign_bit = int(input[0])
    mantissa_int = convertToInt(input[9:])
    real_no = pow(-1, sign_bit) * mantissa_int * pow(2, exponent_unbias)
    return(real_no)


def four_byte_convert(input):
    newString = input[6:8] + input[4:6] + input[2:4] + input[0:2]
    return(bin_to_float(format(int(newString, 16), "032b")))


def bytes(integer):
    return divmod(integer, 0x100)


def updateBoard(port,msg, packet):
    print(msg)
    port.write(packet)
    time.sleep(0.5)


trueDistance = input("True Distance between anchors in meters: ")

antennaDelay = 16800
high, low = bytes(antennaDelay)
updateBoard(serialTagPort, "set Tag antenna delay", bytearray([0x41,0x54,0x03,0x10,high,low,0x0A,0x0D]) )
updateBoard(serialTagPort, "set Tag antenna delay", bytearray([0x41,0x54,0x03,0x0E,high,low,0x0A,0x0D]) )
updateBoard(serialAnchorPort, "set anchor antenna delay", bytearray([0x41,0x54,0x03,0x10,high,low,0x0A,0x0D]) )
updateBoard(serialAnchorPort, "set anchor antenna delay", bytearray([0x41,0x54,0x03,0x0E,high,low,0x0A,0x0D]) )
updateBoard(serialTagPort, "restarting boards", bytearray([0x41, 0x54, 0x01, 0x85, 0x0A, 0x0D]))
updateBoard(serialAnchorPort, "restarting boards", bytearray([0x41, 0x54, 0x01, 0x85, 0x0A, 0x0D]))
time.sleep(2)

iterationNumber = 0
while True:
    print(f"Starting iteration {iterationNumber}")
    total = 0
    i = 0
    while i < 10:
        hexStringStart = serialPort.read(1).hex()
        if hexStringStart == 'ff':
            output = hexStringStart + serialPort.read(59).hex()
            range = four_byte_convert(output[10:18])
            if range <= 30 and range >= -30:
                total = round(float(total + range), 8)
                i = i + 1

    average = total/i
    print(f"Finished iteration {iterationNumber}")
    percentageDifference = (float(trueDistance) - average)/float(trueDistance)
    print(percentageDifference)
    if abs(percentageDifference) <= 0.01:
        print("Finished Calibrating to within 0.01% of true distance")
        print(f"Iteration Number {iterationNumber}")
        print(f"Final antennaDelay {antennaDelay}")
        break

    if abs(percentageDifference) >= 0.6:
        changeOffset = percentageDifference * 500
    elif abs(percentageDifference) >= 0.2:
        if percentageDifference <= 0:
            changeOffset = -5
        else:
            changeOffset = 5
    else:
        if percentageDifference <= 0:
            changeOffset = -1
        else:
            changeOffset = 1

    antennaDelay = int(antennaDelay - changeOffset)
    print(f"New antenna delay {antennaDelay}")
    high, low = bytes(antennaDelay)
    updateBoard(serialTagPort, "set Tag antenna delay", bytearray([0x41,0x54,0x03,0x10,high,low,0x0A,0x0D]) )
    updateBoard(serialTagPort, "set Tag antenna delay", bytearray([0x41,0x54,0x03,0x0E,high,low,0x0A,0x0D]) )
    updateBoard(serialAnchorPort, "set anchor antenna delay", bytearray([0x41,0x54,0x03,0x10,high,low,0x0A,0x0D]) )
    updateBoard(serialAnchorPort, "set anchor antenna delay", bytearray([0x41,0x54,0x03,0x0E,high,low,0x0A,0x0D]) )
    updateBoard(serialTagPort, "restarting boards", bytearray([0x41, 0x54, 0x01, 0x85, 0x0A, 0x0D]))
    updateBoard(serialAnchorPort, "restarting boards", bytearray([0x41, 0x54, 0x01, 0x85, 0x0A, 0x0D]))
    time.sleep(2)
    iterationNumber = iterationNumber + 1
