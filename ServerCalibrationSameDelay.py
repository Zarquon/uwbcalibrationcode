import socket
import serial
import time

host='192.168.0.23' #client ip
port = 4000


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((host,port))

portName = 'COM6'
serialPort = serial.Serial(port=portName, baudrate=115200, bytesize=8, timeout=2 ,stopbits=serial.STOPBITS_ONE)


def updateBoard(msg, packet):
    print(msg)
    serialPort.write(packet)
    time.sleep(0.5)

def bytes(integer):
    return divmod(integer, 0x100)

while True:
    print("waiting")
    data, addr = s.recvfrom(1024)
    data = data.decode('utf-8')
    print("Received from server: " + data)

    high, low = bytes(int(data))
    updateBoard("set new antenna delay", bytearray([0x41,0x54,0x03,0x10,high,low,0x0A,0x0D]) )
    updateBoard("set new antenna delay", bytearray([0x41,0x54,0x03,0x0E,high,low,0x0A,0x0D]) )
    updateBoard("restart", bytearray([0x41,0x54,0x01,0x85,0x0A,0x0D]) )

    time.sleep(3)


    message = 'test'
    s.sendto(message.encode('utf-8'), addr)

    data, addr = s.recvfrom(1024)
    data = data.decode('utf-8')
    print("Received from server: " + data)
