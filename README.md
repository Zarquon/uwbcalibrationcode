# UWBCalibrationCode


## Description
Code designed to calibrate various UWB boards over a known distance. These programs assume the Tx and Rx antenna delays will be the same.

## Single board calibration
Used to calibrate an anchor using a calibrated tag over a known distance. One laptop required, When running the program enter the distance between the boards as well as the serial port address.

## Two board calibration over WIFI
Used to calibrate two uncalibrated boards over WIFI. Both boards need to be the same type and have a laptop each. The laptops need to be on the same network, and the python Socket library needs to be installed on both computers. Run the Server program first and enter the IP address and port number for communications. Run the Client program and enter the distance between the anchors as well as the client and Server IP address and port.

## Two board calibration over serial
Used to calibrate two uncalibrated boards over serial with one laptop.
In development.

## Authors and acknowledgment
Mitchell Torok
Kai Zhao
